// noinspection JSUnusedGlobalSymbols

import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

export default () => {

    const buildTarget = process.env.BUILD_DEV ? "build/dev" :
                               process.env.BUILD_PROD ? "build/prod" :
                               process.env.BUILD_EXT ? "build/ext" : "";

    return [
        {
            // Build the sf-status-support separately from the other bundles to keep the dependencies
            // clean. In the future we may split this project into multiple smaller projects.
            // Build or copy the resources needed for the status page (contact person Thomas)
            input: 'src/status/sf-status-support.js',
            output: {
                dir: buildTarget,
                format: 'es',
                // sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                // terser({}),
                copy({
                    targets: [
                        {src: "src/status/sf.svg", dest: buildTarget },
                        {src: "src/status/diavolezza.png", dest: buildTarget },
                        {src: "src/status/alvra.png", dest: buildTarget },
                        {src: "src/status/maloja.png", dest: buildTarget },
                        {src: "src/status/furka.png", dest: buildTarget },
                        {src: "src/status/bernina.png", dest: buildTarget },
                        {src: "src/status/cristallina.png", dest: buildTarget },
                        {src: "src/status/PSI_logo.jpg", dest: buildTarget },
                        {src: "src/status/status.html", dest: buildTarget },
                        {src: "src/status/status.js", dest: buildTarget },
                        {src: "src/status/plot.js", dest: buildTarget },
                    ],
                }),
            ]
        },
        {
            // Build or copy the resources needed for the multiple plot page (contact person Nicole)
            input: 'src/multiplot/sf-multiplot-support.js',
            output: {
                dir: buildTarget,
                format: 'es',
                sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                // terser({}),
                copy({
                    targets: [
                        {src: "src/multiplot/multiplot.html", dest: buildTarget },
                        {src: "src/multiplot/multiplot-styles.css", dest: buildTarget },
                    ],
                }),
            ]
        },
        {
            // Build or copy the resources needed for the status overview page (contact person Didier)
            input: 'src/overview/sf-overview-support.js',
            output: {
                dir: buildTarget,
                format: 'es',
                // sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                // terser({}),
                copy({
                    targets: [
                        {src: "src/overview/rf-station.template.html", dest: buildTarget },
                        {src: "src/overview/overview.html", dest: buildTarget },
                        {src: "src/overview/overview-styles.css", dest: buildTarget },
                        {src: "src/overview/sf-overview.svg", dest: buildTarget },
                    ],
                }),
            ]
        },
    ];
}