console.debug( "Executing script in sf-overview-support.js module...");

import {GfaTestPlotBasicXy} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotDualY} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotBasicDatasource} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotDualDatasource} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotDatasourceManager} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestCamera} from "@psi/gfa-test-wc-factory/gfa-test-camera-support.js";
import {GfaTestCameraDatasource} from "@psi/gfa-test-wc-factory/gfa-test-camera-support.js";
import {GfaTestCameraDatasourceManager} from "@psi/gfa-test-wc-factory/gfa-test-camera-support.js";
import {DocumentSupportLoader} from "@psi/wica-js/client-api.js";
import JSON5 from "json5";

export {
    GfaTestPlotBasicXy,
    GfaTestPlotDualY,
    GfaTestPlotBasicDatasource,
    GfaTestPlotDualDatasource,
    GfaTestPlotDatasourceManager,
    GfaTestCamera,
    GfaTestCameraDatasource,
    GfaTestCameraDatasourceManager,
    DocumentSupportLoader,
    JSON5
}
