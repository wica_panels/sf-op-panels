console.debug( "Executing script in sf-multiplot-support.js module...");

import {GfaTestPlotBasicXy} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotBasicDatasource} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {GfaTestPlotDatasourceManager} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import {DocumentSupportLoader} from "@psi/gfa-test-wc-factory/gfa-test-plot-support.js";
import JSON5 from "json5";

export {
    GfaTestPlotBasicXy,
    GfaTestPlotBasicDatasource,
    GfaTestPlotDatasourceManager,
    DocumentSupportLoader,
    JSON5
}
