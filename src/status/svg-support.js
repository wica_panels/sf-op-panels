console.debug( "Executing script in svg-support.js module...");

export {
    update_svg_numeric_value,
    update_svg_numeric_value_pair,
    update_svg_string_value,
    update_svg_endstation_message,
    update_svg_electron_beam_path,
    update_svg_electron_beam_dump_path,
    update_svg_undulator_symbol_photon_beam_shutter_path,
    update_svg_photon_beam_shutter_symbol,
    update_svg_photon_beam_path,
    update_svg_endstation_selection_status,
    update_svg_beamline_status,
};

const DUMP_CHARGE_DETECTION_THRESHOLD_IN_A = 1.0;
const BUNCH_DETECTION_THRESHOLD_IN_PC = 1.0;
const DOUBLE_BUNCH_DETECTION_FRACTION = 0.05;


const WICA_CHANNEL_OK= Symbol();
const WICA_STREAM_ERROR= Symbol();
const WICA_CONNECTION_ERROR= Symbol();

const q1ValueCache = new Map();
const q2ValueCache = new Map();
const rdValueCache = new Map();

const g1ValueCache = new Map();
const g2ValueCache = new Map();
const esValueCache = new Map();

let svgDoc = null;

/**
 * Updates the SVG elements representing an electron beam path segment using the latest channel value
 * obtained from the supplied event.
 *
 * The update is based on the latest channel value and the previously cached values.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleClass the class name of the SVG elements to update.
 * @param q1q2 token indicating which of the electron charge caches to update: 'q1', or 'q2'.
 */
function update_svg_electron_beam_path( event, svgEleClass, q1q2 )
{
    function setAttribute( ele, attr, isTrue )
    {
        ele.setAttribute( attr, isTrue.toString() );
    }

    _init_svg_document_ref();
    const targetElements = svgDoc.getElementsByClassName( svgEleClass );
    if ( targetElements == null ) {
        return;
    }

    [ ...targetElements ].forEach( ( ele ) => {

        if (!_verify_connection_and_channel (event, (status) => _handleConnectionState(ele, status))) {
            return;
        }

        // Update the relevant cache item with the latest value.
        const latestValue = event.channelValueLatest["val"];
        switch( q1q2 )
        {
            case "q1":
                q1ValueCache.set( svgEleClass, latestValue );
                break;
            case "q2":
                q2ValueCache.set( svgEleClass, latestValue );
                break;
        }

        // Using the cached values, determine whether an electron bunch is detected and
        // whether the beam segment is operating in single or double bunch mode.
        const [ q1, q2 ] = [ q1ValueCache.get( svgEleClass ), q2ValueCache.get( svgEleClass ),  rdValueCache.get( svgEleClass ) ];
        const [ qMin, qMax ] = [ Math.min( q1, q2 ), Math.max( q1, q2 ) ];
        const bunch1Detected =  q1 > BUNCH_DETECTION_THRESHOLD_IN_PC;
        const bunch2Detected =  q2 > BUNCH_DETECTION_THRESHOLD_IN_PC;
        const bunchDetected = bunch1Detected || bunch2Detected;
        const singleBunchDetected = bunchDetected && ( qMin <= DOUBLE_BUNCH_DETECTION_FRACTION * qMax );
        const doubleBunchDetected =  bunchDetected && ( qMin > DOUBLE_BUNCH_DETECTION_FRACTION * qMax );
        setAttribute( ele,"data-bunch1-detected", bunch1Detected );
        setAttribute( ele,"data-bunch2-detected", bunch2Detected );
        setAttribute( ele,"data-single-bunch-detected", singleBunchDetected );
        setAttribute( ele,"data-double-bunch-detected", doubleBunchDetected );
        const beamPathColor = doubleBunchDetected ? "darkblue" : singleBunchDetected ? "lightblue" : "darkgray";

        ele.style.setProperty( "--el_gun_path_color", beamPathColor );
        ele.style.setProperty( "--el_chicane_path_color", beamPathColor );
        ele.style.setProperty( "--el_tube_path_color", beamPathColor );
        ele.style.setProperty( "--undulator_el_in_path_color", beamPathColor );
        ele.style.setProperty( "--undulator_el_out_path_color", beamPathColor );
        ele.style.setProperty( "--el_tube_to_athos_path_color", beamPathColor );
        ele.style.setProperty( "--el_beamdump_path_color", beamPathColor );
    } );
}

/**
 * Updates the SVG elements representing the electron beam path to an electron beam dump using the latest
 * channel value obtained from the supplied event.
 *
 * The update is based on the latest channel value and the previously cached values.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleClass the class name of the SVG elements to update.
 * @param q1q2rD token indicating which of the electron charge caches to update: 'q1', 'q2' or 'rD'.
 */
function update_svg_electron_beam_dump_path( event, svgEleClass, q1q2rD )
{
    function setAttribute( ele, attr, isTrue )
    {
        ele.setAttribute( attr, isTrue.toString() );
    }

    _init_svg_document_ref();
    const targetElements = svgDoc.getElementsByClassName( svgEleClass );
    if ( targetElements == null ) {
        return;
    }

    [ ...targetElements ].forEach( ( ele ) => {

        if (!_verify_connection_and_channel (event, (status) => _handleConnectionState( ele, status ) ) ) {
            return;
        }

        const latestValue = event.channelValueLatest["val"];
        switch( q1q2rD )
        {
            case "q1":
                q1ValueCache.set( svgEleClass, latestValue );
                break;
            case "q2":
                q2ValueCache.set( svgEleClass, latestValue );
                break;
            case "rd":
                rdValueCache.set( svgEleClass, latestValue );
                break;
        }

        const [ q1, q2, rd ] = [ q1ValueCache.get( svgEleClass ), q2ValueCache.get( svgEleClass ),  rdValueCache.get( svgEleClass ) ];
        const [ qMin, qMax ] = [ Math.min( q1, q2 ), Math.max( q1, q2 ) ];
        const dumpChargeDetected = ( rd > DUMP_CHARGE_DETECTION_THRESHOLD_IN_A );
        const bunch1Detected =  q1 > BUNCH_DETECTION_THRESHOLD_IN_PC;
        const bunch2Detected =  q2 > BUNCH_DETECTION_THRESHOLD_IN_PC;
        const bunchDetected = bunch1Detected || bunch2Detected;
        const singleBunchDetected = bunchDetected && ( qMin <= DOUBLE_BUNCH_DETECTION_FRACTION * qMax );
        const doubleBunchDetected =  bunchDetected && ( qMin > DOUBLE_BUNCH_DETECTION_FRACTION * qMax );

        setAttribute( ele,"data-bunch1-detected", bunch1Detected );
        setAttribute( ele,"data-bunch2-detected", bunch2Detected );
        setAttribute( ele,"data-single-bunch-detected", singleBunchDetected );
        setAttribute( ele,"data-double-bunch-detected", doubleBunchDetected );
        setAttribute( ele,"data-dump-charge-present", dumpChargeDetected );
        const beamPathColor = dumpChargeDetected ? ( doubleBunchDetected ? "darkblue" : singleBunchDetected ? "lightblue" : "darkgray" ) : "darkgray";
        ele.style.setProperty( "--el_beamdump_path_color", beamPathColor );
    } );
}


function update_svg_photon_beam_path( event, svgEleClass, g1g2eS, targetValue="dont-care" )
{
    function setAttribute( ele, attr, isTrue )
    {
        ele.setAttribute( attr, isTrue.toString() );
    }

    _init_svg_document_ref();
    const targetElements = svgDoc.getElementsByClassName( svgEleClass );
    if ( targetElements == null ) {
        return;
    }

    [ ...targetElements ].forEach( ( ele ) => {

        if (!_verify_connection_and_channel (event, (status) => _handleConnectionState( ele, status ) ) ) {
            return;
        }

        const latestValue = event.channelValueLatest["val"];
        switch( g1g2eS )
        {
            case "g1":
                g1ValueCache.set( svgEleClass, latestValue );
                break;
            case "g2":
                g2ValueCache.set( svgEleClass, latestValue );
                break;
            case "es":
                esValueCache.set( svgEleClass, latestValue );
                break;
        }

        const [ g1, g2, es ] = [ g1ValueCache.get( svgEleClass ), g2ValueCache.get( svgEleClass ), esValueCache.get( svgEleClass ) ];
        const photonsDetected = g1 > 1;
        const beamShutterOpen = g2 !== "TRUE";
        const targetPathSelected = ( es === targetValue ) || ( targetValue === "dont-care" );
        const photonPathActive = photonsDetected && beamShutterOpen && targetPathSelected;
        setAttribute( ele,"data-photon-path-active", photonPathActive );
    } );
}

/**
 * Updates the selection states of the SVG elements representing the beamline endstations using the latest value
 * obtained from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleClass the class name of the SVG endstation elements to update.
 */
function update_svg_endstation_selection_status( event, svgEleClass )
{
    function setAttribute( ele, attr, beamlineState )
    {
        ele.setAttribute( attr, beamlineState.toLowerCase() );
    }

    _init_svg_document_ref();
    const targetElements = svgDoc.getElementsByClassName( svgEleClass );
    if ( targetElements == null ) {
        return;
    }

    [ ...targetElements ].forEach( ( ele ) => {

        if (!_verify_connection_and_channel(event, (status) => _handleConnectionState( ele, status ) ) ) {
            return;
        }
        const beamlineState = event.channelValueLatest["val"];
        setAttribute( ele,"data-endstation-selected", beamlineState );
    } );
}


/**
 * Updates the SVG symbol for a beamline undulator to show the latest state photon shutter path state using the value
 * obtained from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleId reference to the SVG beam undulator element to update.
 */
function update_svg_undulator_symbol_photon_beam_shutter_path( event, svgEleId, )
{
    function setAttribute( ele, attr, isTrue )
    {
        ele.setAttribute( attr, isTrue.toString() );
    }

    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if (!_verify_connection_and_channel (event, (status) => _handleConnectionState( targetElement, status ) ) ) {
        return;
    }

    const latestValue = event.channelValueLatest["val"];
    const photonsDetected = latestValue > 1;
    setAttribute( targetElement,"data-photon-beam-shutter-path-active", photonsDetected );
}

/**
 * Updates the SVG symbol representing a photon beam shutter using the latest channel value from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleId reference to the SVG beam shutter element to update.
 */
function update_svg_photon_beam_shutter_symbol( event, svgEleId )
{
    function setAttribute( ele, attr, isTrue )
    {
        ele.setAttribute( attr, isTrue.toString() );
    }

    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if (!_verify_connection_and_channel(event, (status) => _handleConnectionState( targetElement, status))) {
        return;
    }
    const beamShutterOpen = event.channelValueLatest["val"] !== "TRUE";
    setAttribute( targetElement,"data-beam-shutter-open", beamShutterOpen );
}

/**
 * Updates the SVG text element using the latest using the latest numeric value obtained from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleId reference to the SVG text element to update.
 * @param fractionDigits number of digits to display after the decimal point.
 * @param optUnits optional flag to include units in the displayed value.
 */
function update_svg_numeric_value( event, svgEleId, fractionDigits, optUnits = false )
{
    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! _verify_connection_and_channel( event, ( status ) => _handleConnectionState( targetElement, status ) ) )
    {
        return;
    }

    const value = event.channelValueLatest.val.toFixed( fractionDigits );
    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = value + " " + units;
}


/**
 * Updates the SVG text element using the latest numeric values obtained from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleId reference to the SVG text element to update.
 * @param position 'L' or 'R' to indicate which numeric value to update
 * @param fractionDigits number of digits to display after the decimal point.
 * @param units optional flag to include units should be included in the displayed values.
 */
function update_svg_numeric_value_pair( event, svgEleId, position, fractionDigits, units= "" )
{
    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! _verify_connection_and_channel( event, ( status ) => _handleConnectionState( targetElement, status ) ) )
    {
        return;
    }

    // Regex pattern to match the input format:
    // ^ - Start of the string
    // (\d+(\.\d+)? - A floating point number captured as the first group (first numeric value)
    // \s\/\s - A space, followed by '/', followed by another space
    // (\d+(\.\d+)? - A floating point number, captured as the second group (second numeric value)
    // (\s[a-zA-Z0-9]+)? - An optional part: a space followed by one or more alphanumeric characters, captured as the third group (optional alphanumeric value)
    // $ - End of the string
    const pattern = /^(\d+(\.\d+)?)\s\/\s(\d+(\.\d+)?)(\s[a-zA-Z0-9]+)?$/;
    const existingContent = targetElement.textContent;
    const match = existingContent.match( pattern );

    if (! match )
    {
        targetElement.textContent = "0.0 / 0.0 units";
        return;
    }

    const firstNumericValue = match[ 1 ];
    const secondNumericValue = match[ 3 ];
    const latestValue = event.channelValueLatest.val.toFixed( fractionDigits );
    const delimiter = units.length === 0 ? "" : " ";
    switch ( position )
    {
        case 'L':
            targetElement.textContent = latestValue + " / " + secondNumericValue + delimiter + units
            break;

        case 'R':
            targetElement.textContent = firstNumericValue + " / " + latestValue + delimiter + units
            break;
    }
}

/**
 * Updates the SVG text element using the latest string value obtained from the supplied event.
 *
 * @param event the event object carrying the latest channel value.
 * @param svgEleId reference to the SVG text element to update.
 * @param optUnits optional flag to include units in the displayed value.
 */
function update_svg_string_value( event, svgEleId, optUnits = false )
{
    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! _verify_connection_and_channel( event, ( status ) => _handleConnectionState( targetElement, status ) ) )
    {
        return;
    }

    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = event.channelValueLatest[ "val" ] + " " + units ;
}

function update_svg_endstation_message( event, svgTargetEleId, svgMessageSourceEleId, targetEndstation )
{
    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgTargetEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! _verify_connection_and_channel( event, ( status ) => _handleConnectionState( targetElement, status ) ) )
    {
        return;
    }

    const matchElement = svgDoc.getElementById( svgMessageSourceEleId );
    if (  matchElement.textContent === targetEndstation )
    {
        targetElement.textContent = event.channelValueLatest[ "val" ];
    }
}

function update_svg_beamline_status( event, svgEleId )
{
    _init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! _verify_connection_and_channel( event, ( status ) => _handleConnectionState( targetElement, status ) ) )
    {
        return;
    }

    const beamlineState = event.channelValueLatest[ "val" ];
    targetElement.textContent = beamlineState === "Invalid" ? "No endstation selected" : beamlineState;
}


function _verify_connection_and_channel( event, callbackHandler )
{
    const wicaStreamState = event.target.dataset.wicaStreamState;
    const wicaChannelConnectionState = event.target.dataset.wicaChannelConnectionState;

    // The wicaStreamState attribute looks typically like this: 'opened-<n>', where <n> is the connection attempt.
    if ( !wicaStreamState.includes( "opened" ) ) {
        callbackHandler( WICA_STREAM_ERROR );
        return false;
    }

    // The wicaChannelConnectionState attribute looks like this when the underlying channel is connected: 'connected'.
    if  ( wicaChannelConnectionState !== "connected" ) {
        callbackHandler( WICA_CONNECTION_ERROR );
        return false;
    }
    callbackHandler( WICA_CHANNEL_OK );
    return true;
}

function _handleConnectionState( targetElement, status )
{
    switch ( status )
    {
        case WICA_STREAM_ERROR:
            targetElement.textContent = "Wica Stream NC";
            targetElement.style.fill = "lightgray";
            break;

        case WICA_CONNECTION_ERROR:
            targetElement.textContent = "EPICS Channel NC";
            targetElement.style.fill = "lightgray";
            break;

        case WICA_CHANNEL_OK:
            targetElement.style.fill = "black";
            break;
    }
}

function _init_svg_document_ref()
{
    if ( svgDoc === null ) {
        const svgElement = document.getElementById( "svgHostElement" );
        svgDoc = svgElement.contentDocument;
    }
}
