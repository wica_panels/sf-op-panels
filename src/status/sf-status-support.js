console.debug( "Executing script in sf-status-support.js module...");

import * as SF_STATUS_SVG_FUNCS from "./svg-support.js"
import * as SF_STATUS_PLOT_FUNCS from "./plot-support.js";

export {
    SF_STATUS_SVG_FUNCS,
    SF_STATUS_PLOT_FUNCS
}
