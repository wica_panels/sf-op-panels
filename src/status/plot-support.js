// noinspection JSUnresolvedReference,DuplicatedCode,JSUnresolvedReference

console.debug( "Executing script in plot-support.js module...");

import JSON5 from "json5";
import Chart from 'chart.js/auto';

export {
    do_plot
};

const PLOT_CHANNEL_OK= Symbol();
const WICA_STREAM_ERROR= Symbol();
const WICA_CONNECTION_ERROR= Symbol();

let svgDoc = null;
let myPlot = null;


function do_plot()
{
    init_svg_document_ref();
    if ( myPlot === null )
    {
        init_plot();
    }
    else
    {
        refresh_plot();
    }
}

function init_plot()
{
    const channelEle1 = document.getElementById( "athos_x_axis" );
    const channelEle2 = document.getElementById( "athos_y_axis" );
    const channelEle3 = document.getElementById( "aramis_x_axis" );
    const channelEle4 = document.getElementById( "aramis_y_axis" );
    if ( ! verify_connection_and_channels( channelEle1, channelEle2, channelEle3, channelEle4, (status) => handleConnectionState( status ) ) )
    {
        return
    }

    const yValues1 = getWicaLatestValue( "athos_y_axis"  );
    const xValues1 = getWicaLatestValue( "athos_x_axis"  );
    const scatterData1 = xValues1.map((x, i) => ({ x: x, y: yValues1[i] }));

    const yValues2 = getWicaLatestValue( "aramis_y_axis"  );
    const xValues2 = getWicaLatestValue( "aramis_x_axis"  );
    const scatterData2 = xValues2.map((x, i) => ({ x: x, y: yValues2[i] }));

    const plotEle = document.getElementById("plotCanvas" );
    const ctx = plotEle.getContext("2d");
    myPlot = createPlot( ctx, "Photon Pulse Energy in µJ", "Past hours", "µJ", scatterData1, scatterData2 );

    // Tragically because safari web browsers on iOS do not properly support the rendering of foreignObject elements
    // we need to convert our plot to image data which we then directly insert on the SVG canvas. This approach
    // is crude, but effective and since the plot is only be updated every few seconds, the performance impact is
    // acceptable.
    const base64Canvas = plotEle.toDataURL("image/png");
    const svgImage = svgDoc.getElementById("svgImage" );

    svgImage.setAttributeNS('http://www.w3.org/1999/xlink', 'href', base64Canvas );
}

function refresh_plot()
{
    const channelEle1 = document.getElementById( "athos_x_axis" );
    const channelEle2 = document.getElementById( "athos_y_axis" );
    const channelEle3 = document.getElementById( "aramis_x_axis" );
    const channelEle4 = document.getElementById( "aramis_y_axis" );
    if ( ! verify_connection_and_channels( channelEle1, channelEle2, channelEle3, channelEle4, (status) => handleConnectionState( status ) ) )
    {
        return
    }

    const yValues1 = getWicaLatestValue( "athos_y_axis"  );
    const xValues1 = getWicaLatestValue( "athos_x_axis"  );
    let scatterData1 = xValues1.map((x, i) => ({ x: x, y: yValues1[i] }));

    const yValues2 = getWicaLatestValue( "aramis_y_axis"  );
    const xValues2 = getWicaLatestValue( "aramis_x_axis"  );
    let scatterData2 = xValues2.map((x, i) => ({ x: x, y: yValues2[i] }));

    updatePlot( myPlot, scatterData1, scatterData2 );

    const plotEle = document.getElementById("plotCanvas" );
    const base64Canvas = plotEle.toDataURL("image/png");
    const svgImage = svgDoc.getElementById("svgImage" );

    svgImage.setAttributeNS('http://www.w3.org/1999/xlink', 'href', base64Canvas);
}

function createPlot( plotEle, plotTitle, plotXAxisTitle, plotYAxisTitle, scatterData1, scatterData2 )
{
    Chart.defaults.font.size = 20;
    Chart.defaults.plugins.tooltip.enabled = false;

    const data = {

        datasets: [{
            label: 'Athos',
            pointRadius: 1,
            data: scatterData1,
            borderColor: 'orangered',
            showLine: true,
            borderWidth: 3,
            yAxisID: 'y'
        },
        {
            label: 'Aramis                Past hours',
            pointRadius: 1,
            data: scatterData2,
            borderColor: 'royalblue',
            showLine: true,
            borderWidth: 3,
            yAxisID: 'y1'
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            responsive: false,   // Note: this must be false to allow the canvas to be converted to an image
            maintainAspectRatio: true,
            animation: false,    // Note: this must be false to allow the canvas to be converted to an image
            plugins: {
                legend: {
                    display: true,
                    position: 'bottom',
                    align: 'start',
                    labels: {
                        boxWidth: 3,
                        fontSize: 3,
                        padding: 15,
                        usePointStyle: false
                    },
                },
                title: {
                    display: true,
                    text: plotTitle
                }
            },
            scales: {
                x: {
                    grid: {
                        display: true,
                        color: "rgba(0, 0, 0, 0.2)",
                        lineWidth: 3 // Set grid line width
                    },
                    title: {
                        text: plotXAxisTitle,
                        display: false
                    },
                    type: 'linear',
                    position: 'bottom',
                    align: 'end'
                },
                y: {
                    ticks: {
                        color: 'orangered'
                    },
                    position: 'left',
                    grid: {
                        display: true,
                        color: "rgba(0, 0, 0, 0.2)",
                        lineWidth: 3 // Set grid line width
                    },
                    beginAtZero: false,
                    min: 0,
                    max: 6000,
                    title: {
                        display: false
                    },
                },
                y1: {
                    ticks: {
                        color: 'royalblue'
                    },
                    position: 'right',
                    grid: {
                        display: false,
                    },
                    beginAtZero: false,
                    min: 0,
                    max: 1600,
                    title: {
                        display: false
                    },

                },
            },
        }
    }
    return new Chart( plotEle, config );
}

function updatePlot( chart, scatterData1, scatterData2 )
{
    chart.data.datasets[0].data = scatterData1;
    chart.data.datasets[1].data = scatterData2;
    chart.update();
}

function getWicaLatestValue( wicaDataEleId )
{
    const wicaDataEle = document.getElementById( wicaDataEleId );
    const wicaChannelValueLatestAsString = wicaDataEle.dataset.wicaChannelValueLatest;

    // If data is not yet available return an empty array
    if ( wicaChannelValueLatestAsString === undefined ) {
        return [];
    }
    const wicaChannelValueLatest = JSON5.parse( wicaChannelValueLatestAsString );
    return wicaChannelValueLatest.val;
}

function verify_connection_and_channels( wicaDataEleId1, wicaDataEleId2, wicaDataEleId3, wicaDataEleId4, callbackHandler )
{

    // Simply choose the first element as representative of the connection state of the stream
    // The wicaStreamState attribute looks typically like this: 'opened-<n>', where <n> is the connection attempt.
    const {wicaStreamState} = wicaDataEleId1.dataset;
    if ( !wicaStreamState.includes( "opened" ) ) {
        callbackHandler( WICA_STREAM_ERROR );
        return false;
    }

    // Check the connection state of all the channels
    const wicaChannelConnectionState1 = wicaDataEleId1.dataset.wicaChannelConnectionState;
    const wicaChannelConnectionState2 = wicaDataEleId2.dataset.wicaChannelConnectionState;
    const wicaChannelConnectionState3 = wicaDataEleId3.dataset.wicaChannelConnectionState;
    const wicaChannelConnectionState4 = wicaDataEleId4.dataset.wicaChannelConnectionState;
    if  ( ( wicaChannelConnectionState1 !== "connected" )  || ( wicaChannelConnectionState2 !== "connected" )||
          ( wicaChannelConnectionState3 !== "connected" )  || ( wicaChannelConnectionState4 !== "connected" ) )
    {
        callbackHandler( WICA_CONNECTION_ERROR );
        return false;
    }

    // If we get here all is well, we have the information to continue creating a plot
    callbackHandler( PLOT_CHANNEL_OK );
    return true;
}

function handleConnectionState( status )
{
    const plotCoverTextEle = svgDoc.getElementById( "plot_cover_text" );
    const plotCoverRectEle = svgDoc.getElementById( "plot_cover" );

    switch ( status )
    {
        case WICA_STREAM_ERROR:
            plotCoverTextEle.textContent = "Wica Stream Not Connected (NC)";
            plotCoverTextEle.style.opacity = "0.9";
            plotCoverRectEle.style.opacity = "0.9";
            break;

        case WICA_CONNECTION_ERROR:
            plotCoverTextEle.textContent = "EPICS Channel(s) Not Connected (NC)";
            plotCoverTextEle.style.opacity = "0.9";
            plotCoverRectEle.style.opacity = "0.9";
            break;

        case PLOT_CHANNEL_OK:
            plotCoverTextEle.textContent = "";
            plotCoverTextEle.style.opacity = "0";
            plotCoverRectEle.style.opacity = "0";
            break;
    }
}

function init_svg_document_ref()
{
    if ( svgDoc === null ) {
        const svgElement = document.getElementById( "svgHostElement" );
        svgDoc = svgElement.contentDocument;
    }
}
