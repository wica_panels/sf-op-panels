# Overview

This is the **sf-op-panels** git repository which provides a set of wica panels for visualising SwissFEL's 
operational state to the machine operators and other stakeholders.

Currently (2024-11-13) the project manages the Wica pages related to the following PSI endpoints:

* https://gfa-wica.psi.ch/sf/op/status.html - A high level status page. Main Stakeholder: [Thomas Schietinger](https://intranet.psi.ch/en/people/thomas-schietinger)
* https://gfa-wica.psi.ch/sf/op/overview.html - A more detailed overview page. Main stakeholder: [Didier Voulot](https://intranet.psi.ch/en/people/didier-voulot)
* https://gfa-wica.psi.ch/sf/op/multiplot.html - A page showing multiple plots related to the machine. Main stakeholder: [Nicole Hiller](https://intranet.psi.ch/en/people/nicole-hiller)

The endpoints are also reachable from outside PSI by dropping the 'gfa-' prefix in the above links. Example: https://wica.psi.ch/sf/op/status.html


In the future we may consolidate these pages or amalgamate them into a more logical structure with different http endpoints.

For further information please contact:
* [Simon Rees](https://intranet.psi.ch/en/people/simon-rees)


## Repository Organisation

The intention is simplicity and that you normally work on the **master branch** of the repository.

The following branches are additionally defined, but you don't normally need to look at them as they are used solely to manage
the autodeployment aspects.

* [dist_dev](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_dev) - Use for deployments to [Wica Development Server](https://gfa-wica-dev.psi.ch)
* [dist_prod](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_prod) - Use for deployments to [Wica Production Server](https://gfa-wica.psi.ch)
* [dist_ext](https://gitlab.psi.ch/wica_panels/sf-op-panels/-/tree/dist_ext) - Use for deployments to [Wica External Server](https://wica.psi.ch)

# How to Develop

The project is an NPM project which uses the rollup.js package to create a single support file ('sf-op-support.js')
for inclusion in the html source files.

This project supports three build and dist areas.

* The build/dev and dist_dev filesystem areas are used for deploying to the [Wica Development Server](https://gfa-wica-dev.psi.ch/).
* The build/prod and dist_prod filesystem areas are used for deploying to the [Wica Production Server](https://gfa-wica.psi.ch/).
* The build/ext and dist_ext filesystem areas are  used for deploying to the [Wica External Server](https://wica.psi.ch/).

## Setting up the Working Environment

You will need a command line prompt with [nodejs](https://nodejs.org/en/) installed on your system and available on
your command line.

Since **November 2024** the node environment is available on the SF development machines (**sf-lc.psi.ch**, **sf-lca.psi.ch**)

You will need git.

After using git to clone the repository you will need to install the project's dependencies:
```
  npm install
```

Then you will need to create worktrees in your local directory to track the remote branches on the GitLab Server.
```
  npm run worktrees:create
```

## How to edit

Use your favourite editor to edit the source files in the /src directory.

## How to build and deploy

The npm 'xxx:build' targets create the directories in the build area from the files in the src directory. You run them as follows:
```
  npm run dev:build
  npm run prod:build
  npm run ext:build
```

The npm 'xxx:publish' targets take the directories in the build areas, commits them to the local repository deployment
branches and then pushes the changes to the corresponding branches to the GitLab Server from where they are autodeployed
to the Wica Development, Production or External servers.
```
  npm run dev:publish
  npm run prod:publish
  npm run ext:publish
```

Rather than laboriously clicking the individual run targets you can perform the build-and-deploy operation as a single
step using the following targets:
```
  npm run dev
  npm run prod
  npm run ext
```
The whole process takes around 30s. This seems to be an inevitable consequence of repackaging the plotting library 
into a single support file.

## Quick Deployment - Republishing

If your changes are only to the HTML or CSS files rather than to the project's JS files you only need to do the
build step described above once. Thereafter, you can use the much faster republication targets:

```
  npm run dev:republish
  npm run prod:republish
  npm run prod:republish
```

# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.

