# Overview

This log describes the functionality of tagged versions within the repository.

# Tags

* [1.0.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.0.0)
  * Initial release. Ready for further development.
  * Provides a simple [panel](https://wica.psi.ch/sf/op/nicole.html) showing the state of various EPICS channels and with a couple of plots.

* [1.1.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.1.0)
  * Now provides six plots which render on a variety of different mobile, tablet and desktop platforms.
  * Various cleanups to try to make the deployment workflow easier.
  * Improved the documentation.

* [1.1.1](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.1.1)
  * BUG FIX: Fix regression bug caused by removal of wicq library. This was preventing the text labels from updating.

* [1.2.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.2.0)
  * CHORE: Request from Nicole. Change channel names xxx::PULSE-CHARGE-BUFFER -> xxx::PULSE-CHARGE-GUN-BUFFER.

* [1.3.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.3.0)
  * Add initial support for the SwissFEL Synoptik plot. 

* [1.4.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.4.0)
  * Add placeholder images for other endstations.
  * Retired use of motor position to determine ARAMIS sactive endstation.

* [1.5.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.5.0)
  * ENHANCEMENT: Upgrade images to those provided by Thomas.
  * CHORE: Upgrade chicane energies to use PV's that are consistent with other energy channels.
  * ENHANCEMENT: Switch to fixed y-axis plot scaling (Athos=6000, Aramis-1600).
  * ENHANCEMENT: Create release 1.5.0

* [1.6.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.6.0)
  * CHORE: Eliminate old images for end stations.
  * CHORE: Change name beamline -> undulator. Add support for electron out path.
  * CHORE: Retire undulator photon beamdump as such a component does not exist.
  * CHORE: Improve component positioning.
  * CHORE: Improve electron path section naming ordering.
  * CHORE: Improve electron path section label naming.
  * CHORE: Change name beamline -> undulator.
  * CHORE: Centre labels.
  * ENHANCEMENT: Add support for electron beampath dynamic visualisation.
  * ENHANCEMENT: Add support for nerd corner aka expert info.
  * ENHANCEMENT: Set plot background to white.
  * ENHANCEMENT: Update SVG to show undulator beamdumps as separate.
  * ENHANCEMENT: Improvements to "Nerd Corner" layout.
  * ENHANCEMENT: Eliminate "Charge" label.
  * ENHANCEMENT: Add support for dynamically colourising path to undulator beam dumps.
  * ENHANCEMENT: Add support for reporting beam stability in terms of bunch pairs.
  * ENHANCEMENT: Show expert info with 2 digits of decimal precision.
  * ENHANCEMENT: Show expert info arrival time with 4 digits of precision.
  * ENHANCEMENT:Create release 1.6.0


* [1.7.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.7.0)
  * CHORE: Fix undulator symbol title.
  * ENHANCEMENT: Add support for photon beam shutter.
  * CHORE: Refactor code. Not yet stable.
  * CHORE: Refactor code. Ready for testing.
  * BUG_FIX: Fix typo in channel name.
  * BUG_FIX: Fix aramis beam shutter
  * BUG_FIX: Fix beam shutters.
  * BUG FIX: Fix Athos message text.* 
  * ENHANCEMENT: Create release 1.7.0


* [1.8.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.8.0)
  * CHORE: Create constants for electron bunch detection algorithm.
  * BUG FIX: correct channel names associated with Aramis undulator.
  * CHORE: Improve SVG data-attributes to assist debugging.
  * CHORE: Improved name consistency: '--el_beamline_to_athos_path_color' -> '--el_tube_to_athos_path_color'.
  * CHORE: Algorithm improvements.
  * CHORE: Modernise build environment.
  * BUG FIX: Create release 1.8.0


### [1.9.0](https://gitlab.psi.ch/wica_panels/sf-op-panels/tags/1.9.0)

## Main Points:
- Support for SwissFEL overview displays now centres around three panels: 'status', 'overview' and 'multiplot', with
  corresponding contact partners: 'Thomas Schietinger', 'Didier Voulot' and 'Nicole Hiller'. 
- This release provides the first semi-usable release of a SwissFEL overview display that is approximately like it's 
  caQtDM cousin used in the WBGB control room. Further work will be required to make this display fully operational.

## Detailed commit information:
- CHORE: Fix typo.
- CHORE: Cleanup packaging.
- CHORE: Adapt overview display sot that channel data starts to become readable.
- CHORE: Save latest changes to status display.
- CHORE: Add initial SVG.
- CHORE: Remove redundant definition.
- CHORE: Clean up connection to relevant wica data stream so that page can be deployed in DEV, PROD and EXT environments.
- CHORE: Remove obsolete file.
- CHORE: rename rs82 directory to overview.
- CHORE: Update README with latest information.
- CHORE: Update project structure for cleaner separation of wica pages.
- CHORE: Now explicitly uses PSI's NPM registry.
- CHORE: Update dependencies.
- CHORE: Clean dis_dev.
- CHORE: Save first approximately working overview display. No support yet for Svg.
- CHORE: Save first approximately working multiplot display.
- CHORE: Update dependencies to latest.
- ENHANCEMENT: Update to latest web components (support for dual plot).
- BUG FIX: Add back rendering of multiplot labels.
- ENHANCEMENT: Create release 1.9.0.

### Test Status
- There are no tests available for this release.

### Known Problems
- The SVG graphic used in the overview display is not yet supported.
- The layout breaks when the overview display is used on different devices.

- ### Release Date
- 2024-11-14